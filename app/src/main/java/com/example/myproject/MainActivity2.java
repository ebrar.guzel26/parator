package com.example.myproject;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.provider.FontRequest;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.myproject.adapter.odulAdapter;
import com.example.myproject.object.FaceData;
import com.example.myproject.object.sorular;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity2 extends AppCompatActivity {
ListView listOdul;
odulAdapter odulAdapter;
ArrayList<String> odulDizi;
sorular sorular;
String url = "http://ebrarguzel.com/milyoner/";

    private static final int REQUEST_CALL=1;
    int sorupozisyon = 1;
    View.OnClickListener listener;
    TextView txtSoru,txtsecenek1,txtsecenek2,txtsecenek3,txtsecenek4,txtoyunBitti;
    ImageView yuzdeellibutton,seyircibutton,oynatbutonu,telefonjokeri;
    ArrayList<TextView> txwdiziCevap;
    String cevap;
    FaceData faceData;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        init();
        kmbScnk();
        setUp();
        setClick();
    }

    public void init(){
        odulDizi = new ArrayList<>();
        odulDizi.add("1000000");
        odulDizi.add("500000");
        odulDizi.add("250000");
        odulDizi.add("125000");
        odulDizi.add("64000");
        odulDizi.add("32000");
        odulDizi.add("16000");
        odulDizi.add("8000");
        odulDizi.add("4000");
        odulDizi.add("2000");
        odulDizi.add("1000");
        odulDizi.add("500");
        odulDizi.add("300");
        odulDizi.add("200");
        odulDizi.add("100");

        odulAdapter = new odulAdapter(this,0,odulDizi);

        sorular = new sorular();

        txwdiziCevap = new ArrayList<>();

        faceData = new FaceData();
    }

    public void kmbScnk(){
        listOdul = findViewById(R.id.listOdul);
        txtSoru = findViewById(R.id.txtSoru);
        txtsecenek1 = findViewById(R.id.txtsecenek1);
        txtsecenek2 = findViewById(R.id.txtsecenek2);
        txtsecenek3 = findViewById(R.id.txtsecenek3);
        txtsecenek4 = findViewById(R.id.txtsecenek4);
        txtoyunBitti = findViewById(R.id.txtoyunBitti);
        yuzdeellibutton = findViewById(R.id.yuzdeellibutton);
        seyircibutton = findViewById(R.id.seyircibutton);
        telefonjokeri = findViewById(R.id.telefonjokeri);
        txwdiziCevap.add(txtsecenek1);
        txwdiziCevap.add(txtsecenek2);
        txwdiziCevap.add(txtsecenek3);
        txwdiziCevap.add(txtsecenek4);

        oynatbutonu = findViewById(R.id.oynatbutonu);


    }
    public void setUp(){
        txtoyunBitti.setVisibility(View.GONE);

        listOdul.setAdapter(odulAdapter);

        soruyuGoster();
    }
    public void setClick(){
        listener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                cevapKontrol(((TextView)view));
            }
        };
        for (TextView t:txwdiziCevap){
            t.setOnClickListener(listener);
        }
    }



    public void cevapKontrol(TextView txtw){

        cevap = txtw.getText().toString();
        txtw.setBackgroundResource(R.drawable.grs_secim);

        new CountDownTimer(2000,100){

            @Override
            public void onTick(long l) {

            }
            @Override
            public void onFinish() {
                for (TextView t : txwdiziCevap){
                    String s = t.getText().toString();
                    if (s.equals(sorular.getDogrucevap())){
                        t.setBackgroundResource(R.drawable.grs_yap);
                        break;
                    }
                }
                new CountDownTimer(2000,100){
                    @Override
                    public void onTick(long l) {

                    }

                    @Override
                    public void onFinish() {
                        if(cevap.equals(sorular.getDogrucevap())) {
                            MediaPlayer ring= MediaPlayer.create(MainActivity2.this,R.raw.dogrucevap1);
                            ring.start();
                            sorupozisyon++;
                            if (sorupozisyon > 15) {
                                sorupozisyon = 15;
                                txtoyunBitti.setVisibility(View.VISIBLE);
                                txtoyunBitti.setText(" " + odulDizi.get(0) + "₺ Ödül kazandınız  \n Tebrikler! \n");

                                Retrofit retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
                                myapi api = retrofit.create(myapi.class);
                                Call<Model> call = api.addistatistik(String.valueOf(sorupozisyon),odulDizi.get(0));
                                call.enqueue(new Callback<Model>() {
                                    @Override
                                    public void onResponse(Call<Model> call, Response<Model> response) {

                                    }

                                    @Override
                                    public void onFailure(Call<Model> call, Throwable t) {

                                    }
                                });

                                return;
                            }
                            soruyuGoster();
                        }else {
                            MediaPlayer ring= MediaPlayer.create(MainActivity2.this,R.raw.yanliscevap1);
                            ring.start();
                            txtoyunBitti.setVisibility(View.VISIBLE);
                            int odultut = (sorupozisyon/5)*5;
                            txtoyunBitti.setText(" " + odulDizi.get(14-odultut) + "₺ Ödül kazandınız \n");

                            Retrofit retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
                            myapi api = retrofit.create(myapi.class);
                            Call<Model> call = api.addistatistik(String.valueOf(sorupozisyon),odulDizi.get(14-odultut));
                            call.enqueue(new Callback<Model>() {
                                @Override
                                public void onResponse(Call<Model> call, Response<Model> response) {

                                }

                                @Override
                                public void onFailure(Call<Model> call, Throwable t) {

                                }
                            });


                        }
                    }
                }.start();
            }
        }.start();


    }


    public void setSoru(){
        sorular = faceData.soruOlustur(sorupozisyon);
    }


    public void soruyuGoster(){


        setSoru();
        if(sorupozisyon==4){
            oynatbutonu.setVisibility(View.VISIBLE);
            Resources res = getResources();
            int sound = res.getIdentifier(faceData.mp3tut, "raw", getPackageName());
            MediaPlayer ring= MediaPlayer.create(MainActivity2.this,sound);
            ring.start();
            /*ring.setLooping(true);*/
        }
        else{
            oynatbutonu.setVisibility(View.INVISIBLE);

        }
        txtSoru.setText("Soru "+sorupozisyon+ ": " +sorular.getIcerik());
        ArrayList<String> diziCevap = new ArrayList<>(sorular.getYanlisCevap());
        diziCevap.add(sorular.getDogrucevap());

        Random r = new Random();
        for (int i = 0; i < 5; i++) {
            int vt1 = r.nextInt(diziCevap.size());
            int vt2 = r.nextInt(diziCevap.size());
            String a = diziCevap.get(vt1);
            diziCevap.set(vt1, diziCevap.get(vt2));
            diziCevap.set(vt2, a);
        }

            for (int i=0;i<txwdiziCevap.size();i++){

                txwdiziCevap.get(i).setOnClickListener(listener);
                txwdiziCevap.get(i).setVisibility(View.VISIBLE);
                txwdiziCevap.get(i).setBackgroundResource(R.drawable.grs_btn);
                txwdiziCevap.get(i).setText(diziCevap.get(i));

            }

        odulAdapter.setSoruPozisyon(sorupozisyon);
    }

    public static String birinci="";
    public static String ikinci="";

    String yanlisTut="";

    ArrayList<String> karsilastirma = new ArrayList<>();
    ArrayList<String> fark = new ArrayList<>();

    public String getBirinci() {
        return birinci;
    }

    public void setBirinci(String birinci) {
        this.birinci = birinci;
    }

    public String getIkinci() {
        return ikinci;
    }

    public void setIkinci(String ikinci) {
        this.ikinci = ikinci;
    }

    boolean yuzdeellijokerKontrol = true;

    public void yuzdeellijoker(View view) {

        if(yuzdeellijokerKontrol==false){
            return;
        }
        Random r = new Random();
        int tut = 2;
        do {
            int kontrol = r.nextInt(4);
            TextView t = txwdiziCevap.get(kontrol);

            if(t.getVisibility() == View.VISIBLE && t.getText().toString().equals(sorular.getDogrucevap())== false){
                t.setVisibility(View.INVISIBLE);
                t.setOnClickListener(null);
                tut --;
                yanlisTut += t.toString();

            }


        }while (tut>0);

        birinci = String.valueOf(yanlisTut).substring(102).substring(0,11);
        ikinci = String.valueOf(yanlisTut).substring(216).substring(0,11);

        karsilastirma.add(birinci);
        karsilastirma.add(ikinci);


        yuzdeellijokerKontrol = false;
        yuzdeellibutton.setImageResource(R.drawable.yuzde50harcandi);

        diziFark();



    }

    public void diziFark(){

        fark.add("txtsecenek1");
        fark.add("txtsecenek2");
        fark.add("txtsecenek3");
        fark.add("txtsecenek4");

        fark.removeAll(karsilastirma);

        birinci = fark.toString().substring(1,12);
        ikinci = fark.toString().substring(13).substring(1,12);
    }

    boolean seyircijokerKontrol = true;
    public void seyircijokeri(View view) {
        if(seyircijokerKontrol==false){
            return;
        }

        if(seyircijokerKontrol==true){
            if(yuzdeellijokerKontrol==false){
                for (int i=0;i<txwdiziCevap.size();i++){
                    TextView t = txwdiziCevap.get(i);
                    if(t.getText().toString().equals(sorular.getDogrucevap())){
                        new yuzdeelliwithseyirci(this,i+1).show();
                        break;
                    }
                }

            }
            else {
                for (int i=0;i<txwdiziCevap.size();i++){
                    TextView t = txwdiziCevap.get(i);
                    if(t.getText().toString().equals(sorular.getDogrucevap())){
                        new seyircijokerhakki(this,i+1).show();
                        break;
                    }
                }
            }

        }
        

        birinci = "";
        ikinci = "";
        fark.clear();
        karsilastirma.clear();

        seyircijokerKontrol=false;
        seyircibutton.setImageResource(R.drawable.seyirciharcandi);
    }




    boolean telefonJokerKontrol = true;
    public void telefonjokeri(View view) {
        if(telefonJokerKontrol==false){
            return;
        }
        aramaYap();
        telefonJokerKontrol = false;
        telefonjokeri.setImageResource(R.drawable.telefonjokeriharcandi);

    }





 public void aramaYap(){
     String number = "+905065032020";
     if(number.trim().length()>10){
         if(ContextCompat.checkSelfPermission(MainActivity2.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED){
             ActivityCompat.requestPermissions(MainActivity2.this,new String[]{Manifest.permission.CALL_PHONE},REQUEST_CALL);
         }
         else{
             String dial = "tel:" + number;
             Intent arama = new Intent(Intent.ACTION_CALL, Uri.parse(dial));
             startActivity(arama);
             Intent cikis = new Intent(this, MainActivity2.class);
             new CountDownTimer(5000, 1000) {

                 public void onTick(long millisUntilFinished) {

                 }

                 public void onFinish() {
                     /*startActivity(cikis);*/

                 }

             }.start();

         }
     }
     else{
         Toast.makeText(getApplicationContext(),"Soru Başarıyla silindi.",Toast.LENGTH_SHORT).show();
         //UYGULAMAYA GERİ DÖN VE 30 SN SONRA ARAMAYI SONLANDIR.
     }
 }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_CALL){
            if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                aramaYap();
            }
            else{
                Toast.makeText(getApplicationContext(),"İzin verilmedi.",Toast.LENGTH_SHORT).show();
            }
        }
    }



    public void cikis(View view) {
        AlertDialog.Builder dialogCikis = new AlertDialog.Builder(MainActivity2.this);
        dialogCikis.setTitle("Uyarı");
        dialogCikis.setMessage("Ana menü'ye dönmek istediğinizden emin misiniz?");
        dialogCikis.setIcon(R.drawable.uyari2);


        dialogCikis.setPositiveButton("Evet", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface arg0, int arg1) {
                launchIntent();
            }
        });

        dialogCikis.setNegativeButton("İptal", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface arg0, int arg1) {

            }
        });
        AlertDialog dialog = dialogCikis.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
    }


    @Override
    public void onBackPressed() {
        Intent anaSayfa = new Intent(this,MainActivity.class);
        startActivity(anaSayfa);
    }

    private void launchIntent() {
        Intent cikis = new Intent(this, MainActivity.class);
        startActivity(cikis);
    }

    boolean kontrol = true;
    public void oynatbutonu(View view) {

        if(kontrol == true) {
            Resources res = getResources();
            int sound = res.getIdentifier(faceData.mp3tut, "raw", getPackageName());
            MediaPlayer ring = MediaPlayer.create(MainActivity2.this, sound);
            ring.start();
            kontrol = false;
        }

        new CountDownTimer(20000, 1000) {

            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                kontrol = true;
            }

        }.start();



    }


}