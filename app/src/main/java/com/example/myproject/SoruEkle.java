package com.example.myproject;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SoruEkle extends AppCompatActivity {
    EditText soruid,soru,asikki,bsikki,csikki,dsikki;
    Button insert,update;
    Integer idtut;
    String sorutut,tutasikki,tutbsikki,tutcsikki,tutdsikki;
    String url = "http://ebrarguzel.com/milyoner/";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soru_ekle);

        soruid = findViewById(R.id.soruid);
        soru = findViewById(R.id.soru);
        asikki = findViewById(R.id.asikki);
        bsikki = findViewById(R.id.bsikki);
        csikki = findViewById(R.id.csikki);
        dsikki = findViewById(R.id.dsikki);

        soruid.setVisibility(View.INVISIBLE);


        insert = findViewById(R.id.insert);
        update = findViewById(R.id.update);
        update.setVisibility(View.INVISIBLE);

        Bundle extras = getIntent().getExtras();
        if (extras != null){
            soruid.setVisibility(View.VISIBLE);
            soruid.setEnabled(false);

            insert.setVisibility(View.INVISIBLE);
            update.setVisibility(View.VISIBLE);

            idtut = extras.getInt("id");
            sorutut = extras.getString("soru");
            tutasikki = extras.getString("asikki");
            tutbsikki = extras.getString("bsikki");
            tutcsikki = extras.getString("csikki");
            tutdsikki = extras.getString("dsikki");

            soruid.setText("ID: " + String.valueOf(idtut));
            soru.setText("Soru: " + sorutut);
            asikki.setText("A) " + tutasikki);
            bsikki.setText("B) " + tutbsikki);
            csikki.setText("C) " + tutcsikki);
            dsikki.setText("D) " + tutdsikki);

        }



        insert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                addSoru();
            }
        });
    }


    private void addSoru() {
        if(soru.getText().toString().trim().equals("") || asikki.getText().toString().trim().equals("") || bsikki.getText().toString().trim().equals("") || csikki.getText().toString().trim().equals("") || dsikki.getText().toString().trim().equals("")){
            Toast.makeText(getApplicationContext(),"Lütfen boş alan bırakmayınız!",Toast.LENGTH_SHORT).show();
        }
        else {
            Retrofit retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
            myapi api = retrofit.create(myapi.class);
            Call<Model> call = api.adddata(soru.getText().toString(),asikki.getText().toString(),bsikki.getText().toString(),csikki.getText().toString(),dsikki.getText().toString());
            call.enqueue(new Callback<Model>() {
                @Override
                public void onResponse(Call<Model> call, Response<Model> response) {
                    soru.setText("");
                    asikki.setText("");
                    bsikki.setText("");
                    csikki.setText("");
                    dsikki.setText("");
                    Toast.makeText(getApplicationContext(),"Soru Başarıyla eklendi.",Toast.LENGTH_SHORT).show();
                    Toast.makeText(getApplicationContext(),response.toString(),Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<Model> call, Throwable t) {
                    Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_LONG).show();
                }
            });
        }

    }

    private void getSoru(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
        myapi api = retrofit.create(myapi.class);
        Call<List<Model>> call = api.getSoru();
        call.enqueue(new Callback<List<Model>>() {
            @Override
            public void onResponse(Call<List<Model>> call, Response<List<Model>> response) {
                if(!response.isSuccessful()){
                    soru.setText("Code: " +response.code());
                    return;
                }
                List<Model> sorular = response.body();
                for (Model model:  sorular){
                    String content = "";
                    content+= "id: " + model.getId() + "\n";
                    content+= "soru: " + model.getSoru() + "\n";
                    content+= "asikki: " + model.getAsikki() + "\n";
                    content+= "bsikki: " + model.getBsikki() + "\n";
                    content+= "csikki: " + model.getCsikki() + "\n";
                    content+= "dsikki: " + model.getDsikki() + "\n\n";
                    soru.append(content);

                }
            }

            @Override
            public void onFailure(Call<List<Model>> call, Throwable t) {
                soru.setText(t.getMessage());
            }
        });

    }


    public void deleteSoru(){
            String idParcala = soruid.getText().toString().trim();
            int silinecekID = Integer.parseInt(idParcala.substring(4));
            Retrofit retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
            myapi api = retrofit.create(myapi.class);
            Call<Model> call = api.deleteData(silinecekID);
            call.enqueue(new Callback<Model>() {
                @Override
                public void onResponse(Call<Model> call, Response<Model> response) {
                    Toast.makeText(getApplicationContext(),"Soru Başarıyla silindi.",Toast.LENGTH_SHORT).show();
                }
                @Override
                public void onFailure(Call<Model> call, Throwable t) {
                    Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_LONG).show();
                }
            });
        Panel p = new Panel();
        p.getSoru();

    }

    private void updateSoru(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
        myapi api = retrofit.create(myapi.class);
        Log.d("f",soruid.getText().toString().substring(4)+soru.getText().toString().substring(6)+asikki.getText().toString().substring(3)+bsikki.getText().toString().substring(3)+csikki.getText().toString().substring(3)+dsikki.getText().toString().substring(3));

        //Model m = new Model(Integer.parseInt(soruid.getText().toString().substring(4)),soru.getText().toString().substring(6),asikki.getText().toString().substring(3),bsikki.getText().toString().substring(3),csikki.getText().toString().substring(3),dsikki.getText().toString().substring(3));
        Call<Model> call = api.updateData(Integer.parseInt(soruid.getText().toString().substring(4)),soru.getText().toString().substring(6),asikki.getText().toString().substring(3),bsikki.getText().toString().substring(3),csikki.getText().toString().substring(3),dsikki.getText().toString().substring(3));
        //Call<Model> call = api.updateData(m);


        call.enqueue(new Callback<Model>() {
            @Override
            public void onResponse(Call<Model> call, Response<Model> response) {
                Toast.makeText(getApplicationContext(),"Soru Başarıyla güncellendi.",Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<Model> call, Throwable t) {
                Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_LONG).show();
            }
        });


    }


    @Override
    public void onBackPressed() {
        Intent panel = new Intent(this,Panel.class);
        startActivity(panel);
    }

    public void soruGuncelle(View view) {
        updateSoru();
    }

}

