package com.example.myproject;

public class istatistikModel {


    int id;
    String sorunumarasi,odulmiktari,zaman;

    public istatistikModel(int id, String sorunumarasi, String odulmiktari, String zaman) {
        this.id = id;
        this.sorunumarasi = sorunumarasi;
        this.odulmiktari = odulmiktari;
        this.zaman = zaman;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public String getSorunumarasi() {
        return sorunumarasi;
    }

    public void setSorunumarasi(String sorunumarasi) {
        this.sorunumarasi = sorunumarasi;
    }

    public String getOdulmiktari() {
        return odulmiktari;
    }

    public void setOdulmiktari(String odulmiktari) {
        this.odulmiktari = odulmiktari;
    }

    public String getZaman() {
        return zaman;
    }

    public void setZaman(String zaman) {
        this.zaman = zaman;
    }
}
