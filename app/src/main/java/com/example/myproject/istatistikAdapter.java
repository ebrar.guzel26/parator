package com.example.myproject;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.myproject.Model;
import com.example.myproject.R;

import java.util.ArrayList;


public class istatistikAdapter extends ArrayAdapter<istatistikModel> {

    private final LayoutInflater inflater;
    private final Context context;
    private ViewHolder holder;
    private final ArrayList<istatistikModel> istatistigim;

    public istatistikAdapter(Context context, ArrayList<istatistikModel> istatistikler) {
        super(context,0, istatistikler);
        this.context = context;
        this.istatistigim = istatistikler;
        inflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return istatistigim.size();
    }

    @Override
    public istatistikModel getItem(int position) {
        return istatistigim.get(position);
    }

    @Override
    public long getItemId(int position) {
        return istatistigim.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.list_view_istatistik, null);

            holder = new ViewHolder();
            holder.sorunumarasi = (TextView) convertView.findViewById(R.id.sorunumarasi);
            holder.odulmiktari = (TextView) convertView.findViewById(R.id.odulmiktari);
            holder.zaman = (TextView) convertView.findViewById(R.id.zaman);
            convertView.setTag(holder);

        }
        else{

            holder = (ViewHolder)convertView.getTag();
        }

        istatistikModel istatistikmodel = istatistigim.get(position);
        if(istatistikmodel != null){
            int soruNumarasi = Integer.parseInt(istatistikmodel.getSorunumarasi());
            if(soruNumarasi==15){
                holder.sorunumarasi.setText("Yarışmayı kazandınız!!");
                holder.odulmiktari.setText(istatistikmodel.getOdulmiktari() +"₺ Ödül kazandınız" );
                holder.zaman.setText(istatistikmodel.getZaman() + " Tarih ve Saatinde oyun bitti.");
            }
            else{
                holder.sorunumarasi.setText(istatistikmodel.getSorunumarasi() +". Soruda elendiniz");
                holder.odulmiktari.setText(istatistikmodel.getOdulmiktari() +"₺ Ödül kazandınız" );
                holder.zaman.setText(istatistikmodel.getZaman() + " Tarih ve Saatinde oyun bitti.");
            }

        }
        return convertView;
    }


    private static class ViewHolder {
        TextView sorunumarasi;
        TextView odulmiktari;
        TextView zaman;
    }

}
