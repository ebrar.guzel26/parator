package com.example.myproject;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Panel extends AppCompatActivity {


    String url = "http://ebrarguzel.com/milyoner/";
    ListView mlistView;
    private ArrayList<Model> models;
    private soruAdapter adapter;
    ImageView menu;

    int idtut = -1;
    String sorutut ;
    String asikkitut;
    String bsikkitut;
    String csikkitut;
    String dsikkitut;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panel);
        mlistView = findViewById(R.id.mListView);
        menu = findViewById(R.id.menu);
        getSoru();
        listTiklama();
    }
    public void listTiklama(){
        mlistView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> adapter, View v, int position, long id) {
                Model selItem = (Model) adapter.getItemAtPosition(position);
                idtut = selItem.getId();
                sorutut = selItem.getSoru();
                asikkitut = selItem.getAsikki();
                bsikkitut = selItem.getBsikki();
                csikkitut = selItem.getCsikki();
                dsikkitut = selItem.getDsikki();

            }
        });

    }


    public void getSoru(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
        myapi api = retrofit.create(myapi.class);
        Call<List<Model>> call = api.getSoru();
        call.enqueue(new Callback<List<Model>>() {
            @Override
            public void onResponse(Call<List<Model>> call, Response<List<Model>> response) {
                if(!response.isSuccessful()){

                    return;
                }


                models = new ArrayList<Model>();
                mlistView = (ListView) findViewById(R.id.mListView);
                models = (ArrayList<Model>) response.body();
                adapter = new soruAdapter(Panel.this,models);
                mlistView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Model>> call, Throwable t) {

            }
        });

    }

    public void deleteSoru(){
        Intent panel = new Intent(this,Panel.class); // burası yenileme için
        if(idtut!=-1){
            AlertDialog.Builder dialogCikis = new AlertDialog.Builder(Panel.this);
            dialogCikis.setTitle("Uyarı");
            dialogCikis.setMessage("Soruyu silmek istediğinizden emin misiniz?");
            dialogCikis.setIcon(R.drawable.uyari2);


            dialogCikis.setPositiveButton("Evet", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface arg0, int arg1) {
                    Retrofit retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
                    myapi api = retrofit.create(myapi.class);
                    Call<Model> call = api.deleteData(idtut);
                    call.enqueue(new Callback<Model>() {
                        @Override
                        public void onResponse(Call<Model> call, Response<Model> response) {
                            Toast.makeText(getApplicationContext(),"Soru Başarıyla silindi.",Toast.LENGTH_SHORT).show();
                            startActivity(panel); // burasıda devamı
                        }
                        @Override
                        public void onFailure(Call<Model> call, Throwable t) {
                            Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_LONG).show();
                        }
                    });
                    getSoru();
                    idtut=-1;
                }
            });

            dialogCikis.setNegativeButton("İptal", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface arg0, int arg1) {

                }
            });
            AlertDialog dialog = dialogCikis.show();
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
        }
        else{
            Toast.makeText(getApplicationContext(),"İlk önce silinecek soruya tıklayın.",Toast.LENGTH_SHORT).show();
        }

    }

    public void GuncellemeyeGit(){
        if(idtut!=-1){
            Intent i = new Intent(getApplicationContext(), SoruEkle.class);
            i.putExtra("id", (int) idtut);
            i.putExtra("soru",sorutut);
            i.putExtra("asikki",asikkitut);
            i.putExtra("bsikki",bsikkitut);
            i.putExtra("csikki",csikkitut);
            i.putExtra("dsikki",dsikkitut);
            startActivity(i);
        }
        else{
            Toast.makeText(getApplicationContext(),"İlk önce düzenlenecek soruya tıklayın.",Toast.LENGTH_SHORT).show();
        }


    }
    public void soruekle(View view) {
        Intent cikis = new Intent(this, SoruEkle.class);
        startActivity(cikis);
    }

    @Override
    public void onBackPressed() {
        Intent anaSayfa = new Intent(this,MainActivity.class);
        startActivity(anaSayfa);
    }

    public void showPopup(View view) {
        PopupMenu popup = new PopupMenu(this, view);
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.acilirmenu, popup.getMenu());

        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            public boolean onMenuItemClick(MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.soruguncelle:
                        GuncellemeyeGit();
                        return true;
                    case R.id.sorusil:
                        deleteSoru();
                        return true;
                    default:
                        return false;
                }
            }
        });
        popup.show();
    }


}