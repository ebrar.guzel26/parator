package com.example.myproject;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface myapi {

    @FormUrlEncoded
    @POST("insert.php")
    Call<Model> adddata(@Field("soru") String soru, @Field("asikki") String asikki, @Field("bsikki") String bsikki, @Field("csikki") String csikki,
     @Field("dsikki") String dsikki);

    @GET("json.php")
    Call<List<Model>> getSoru();

    @FormUrlEncoded
    @POST("istatistik_ekle.php")
    Call<Model> addistatistik(@Field("sorunumarasi") String sorunumarasi, @Field("odulmiktari") String odulmiktari);

    @GET("istatistik_json.php")
    Call<List<istatistikModel>> getSorunumarasi();

    @DELETE("delete.php?id=")
    Call<Model> deleteData(@Query("id") int id);


    @PUT("update.php?id=&soru=&asikki=&bsikki=&csikki=&dsikki=")
    Call<Model> updateData(@Query("id") int id,@Query("soru") String soru, @Query("asikki") String asikki, @Query("bsikki") String bsikki, @Query("csikki") String csikki, @Query("dsikki") String dsikki);


}
