package com.example.myproject.adapter;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.myproject.R;

import java.util.ArrayList;
import java.util.List;

public class odulAdapter extends ArrayAdapter<String>{
    Context dnm;
    ArrayList<String> dizi;
    int soruPozisyon = 1;
    public odulAdapter(@NonNull Context context, int resource, @NonNull List<String> objects) {
        super(context, resource, objects);
        this.dnm=context;
        this.dizi= new ArrayList<>(objects);
    }
    public void setSoruPozisyon(int soruPozisyon){
        this.soruPozisyon=soruPozisyon;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        if(convertView==null){
            LayoutInflater Inflater = (LayoutInflater)dnm.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = Inflater.inflate(R.layout.odul, null);
        }
        if(dizi.size()>0) {
            int poz = 15 - position;
            TextView txtOdul = convertView.findViewById(R.id.txtOdul);
            if (poz % 5 == 0) {
                txtOdul.setTextColor(Color.parseColor("#FFFFFF"));
            } else {
                txtOdul.setTextColor(Color.parseColor("#FFEB3B"));
            }
            String odultut;
            if (poz / 10 > 0) {
                odultut= "      ";
            }
            else{
                odultut= "      ";
            }
            String textOdul = poz + odultut+" " + dizi.get(position) + " ₺";
            txtOdul.setText(textOdul);

            if(poz == soruPozisyon)
            {
                txtOdul.setBackgroundColor(Color.parseColor("#FFFFFF"));
                txtOdul.setTextColor(Color.parseColor("#FF9800"));
            }
            else{
                txtOdul.setBackgroundColor(Color.parseColor("#00FFEB3B"));
            }
        }
        return convertView;
}}
