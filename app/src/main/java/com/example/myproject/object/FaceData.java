package com.example.myproject.object;

import android.util.Log;

import com.example.myproject.Model;
import com.example.myproject.myapi;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class FaceData {
    String url = "http://ebrarguzel.com/milyoner/";
    public FaceData(){
        soruOlustur1();
    }

    public sorular soruOlustur(int seviye){
        Random r = new Random();
        ArrayList<sorular> arr = soruDizi.get(seviye-1);
        return arr.get(r.nextInt(arr.size()));

    }

    ArrayList<ArrayList<sorular>> soruDizi = new ArrayList<>();
    Random seslisorucek = new Random();
    int i = seslisorucek.nextInt(19);
    public static String mp3tut = "";



    String sesliDizi[][]={
            {"seslisoru1","Bu müziği duyuyorsanız kimlerin bir gösterisini izliyorsunuzdur?","Trapezciler","Matadorlar","Jokeyler","Güreşçiler"},
            {"seslisoru2","Dinlediğiniz ses hangi Susam Sokağı karakterine aittir?","Kurabiye Canavarı","Kurbağacık","Büdü","Kırpık"},
            {"seslisoru3","Dinlediğiniz şarkının türü nedir?","Grunge","Funk","Soul","Reggae"},
            {"seslisoru4","Dinlediğiniz hikayenin sonu hangi cümleyle biter?","Ya tutarsa?","Ye kürküm ye,","Kazan doğurdu","Sen de haklısın"},
            {"seslisoru5","Dinlediğiniz konuşma genellikle hangi tür filmlerde yer alır?","Yeşilçam","Anime","Bollywood","Yeni Dalga"},
            {"seslisoru6","Girişini dinlediğiniz marşta bahsi geçen nehir hangisidir?","Tuna","Vardar","Volga","Meriç"},
            {"seslisoru7","Dinlediğiniz şarkıyı kim seslendirmektedir?","Zeki Müren","Ahmet Özhan","Zekâi Tunca","Yıldırım Gürses"},
            {"seslisoru8","Neşeli Günler filminden dinlediğiniz bu konuşmadaki “Ziya” karakterini canlandıran oyuncu kimdir?","Şener Şen","Tarık Akan","Kemal Sunal","İlyas Salman"},
            {"seslisoru9","Dinlediğiniz “çaydaçıra” türküsü hangi yöreye aittir?","Elazığ","Adana","Şanlıurfa","Adıyaman"},
            {"seslisoru10","Balalayka orkestrasından dinlediğiniz bu şarkı hangi gruba aittir?","The Beatles","The Rolling Stones","Pink Floyd","Deep Purple"},
            {"seslisoru11","Dinlediğiniz Portekiz’e özgü müzik türünün adı nedir?","Fado","Kazaska","Şanson","Rembetiko"},
            {"seslisoru12","Dinlediğiniz Hint enstrümanlarıyla çalınan bu müzik hangi film serisine aittir?","James Bond","Görevimiz Tehlike","Karayip Korsanları","Geleceğe Dönüş"},
            {"seslisoru13","Adamo’dan dinlediğiniz bu şarkının Türkçe versiyonunun adı nedir?","Karlar Düşer","Hayat Bayram Olsa","Her Yerde Kar Var","Sessiz Gemi"},
            {"seslisoru14","Dinlediğiniz şarkı hangi müzik grubuna aittir?","Red Hot Chili Peppers","Daft Punk","Pink Floyd","Pearl Jam"},
            {"seslisoru15","Dinlediğiniz ses hangi spor karşılaşmasına aittir?","Tenis","Voleybol","Bovling","Golf"},
            {"seslisoru16","Dinlediğiniz sesi duyuyorsanız ne tür bir TV programı izliyorsunuzdur?","Çizgi film","Polisiye film","Korku filmi","Belgesel film"},
            {"seslisoru17","Dinlediğiniz çocuk şarkısının adı nedir?","Küçük Kurbağa","Mini Mini Bir Kuş","Daha Dün Annemizin","Bir Küçücük Aslancık Varmış"},
            {"seslisoru18","Dinlediğiniz şarkıyı hangi müzik grubu seslendirmektedir?","No Doubt","Skunk Anansie","All Saints","Evanescence"},
            {"seslisoru19","Bağlama ile çalınan müziğini dinlediğiniz dizinin adı nedir?","Game of Thrones","Vikingler","Homeland","Marco Polo"},
            {"seslisoru20","Gitar yorumunu dinlediğiniz bu müzik hangi filme aittir?","Karayip Korsanları","Truva","Son Mohikan","Indiana Jones"},
    };

    List<Model> soruCek;
    public void soruOlustur1(){

        ArrayList<sorular> sorudizi1= new ArrayList<>();
        sorudizi1.add(birSoruOlustur("Değeri olan şeyleri, kimseleri koruyan veya sayan, iyilikbilir kişiler için kullanılan ifade hangisidir?","Kadirşinas","Müşkülpesent&Nanemolla&Vakur"));
        soruDizi.add(sorudizi1);

        Retrofit retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
        myapi api = retrofit.create(myapi.class);
        Call<List<Model>> call = api.getSoru();
        call.enqueue(new Callback<List<Model>>() {
            @Override
            public void onResponse(Call<List<Model>> call, Response<List<Model>> response) {
                if(!response.isSuccessful()){
                    return;
                }
                soruCek = response.body();
                Random r = new Random();
                int soruSayisi = soruCek.size();

                int a = r.nextInt(soruSayisi);
                ArrayList<sorular> sorudizi2 = new ArrayList<>();
                sorudizi2.add(birSoruOlustur(soruCek.get(a).getSoru(),soruCek.get(a).getAsikki(),soruCek.get(a).getBsikki()+"&"+soruCek.get(a).getCsikki()+"&"+soruCek.get(a).getDsikki()));
                soruDizi.add(sorudizi2);

                a = r.nextInt(soruSayisi);
                ArrayList<sorular> sorudizi3 = new ArrayList<>();
                sorudizi3.add(birSoruOlustur(soruCek.get(a).getSoru(),soruCek.get(a).getAsikki(),soruCek.get(a).getBsikki()+"&"+soruCek.get(a).getCsikki()+"&"+soruCek.get(a).getDsikki()));
                soruDizi.add(sorudizi3);

                ArrayList<sorular> sorudizi4 = new ArrayList<>();
                sorudizi4.add(birSoruOlustur(sesliDizi[i][1],sesliDizi[i][2],sesliDizi[i][3]+"&"+sesliDizi[i][4]+"&"+sesliDizi[i][5]));
                mp3tut = sesliDizi[i][0];
                soruDizi.add(sorudizi4);

                a = r.nextInt(soruSayisi);
                ArrayList<sorular> sorudizi5 = new ArrayList<>();
                sorudizi5.add(birSoruOlustur(soruCek.get(a).getSoru(),soruCek.get(a).getAsikki(),soruCek.get(a).getBsikki()+"&"+soruCek.get(a).getCsikki()+"&"+soruCek.get(a).getDsikki()));
                soruDizi.add(sorudizi5);

                a = r.nextInt(soruSayisi);
                ArrayList<sorular> sorudizi6 = new ArrayList<>();
                sorudizi6.add(birSoruOlustur(soruCek.get(a).getSoru(),soruCek.get(a).getAsikki(),soruCek.get(a).getBsikki()+"&"+soruCek.get(a).getCsikki()+"&"+soruCek.get(a).getDsikki()));
                soruDizi.add(sorudizi6);

                a = r.nextInt(soruSayisi);
                ArrayList<sorular> sorudizi7 = new ArrayList<>();
                sorudizi7.add(birSoruOlustur(soruCek.get(a).getSoru(),soruCek.get(a).getAsikki(),soruCek.get(a).getBsikki()+"&"+soruCek.get(a).getCsikki()+"&"+soruCek.get(a).getDsikki()));
                soruDizi.add(sorudizi7);

                a = r.nextInt(soruSayisi);
                ArrayList<sorular> sorudizi8 = new ArrayList<>();
                sorudizi8.add(birSoruOlustur(soruCek.get(a).getSoru(),soruCek.get(a).getAsikki(),soruCek.get(a).getBsikki()+"&"+soruCek.get(a).getCsikki()+"&"+soruCek.get(a).getDsikki()));
                soruDizi.add(sorudizi8);

                a = r.nextInt(soruSayisi);
                ArrayList<sorular> sorudizi9 = new ArrayList<>();
                sorudizi9.add(birSoruOlustur(soruCek.get(a).getSoru(),soruCek.get(a).getAsikki(),soruCek.get(a).getBsikki()+"&"+soruCek.get(a).getCsikki()+"&"+soruCek.get(a).getDsikki()));
                soruDizi.add(sorudizi9);

                a = r.nextInt(soruSayisi);
                ArrayList<sorular> sorudizi10 = new ArrayList<>();
                sorudizi10.add(birSoruOlustur(soruCek.get(a).getSoru(),soruCek.get(a).getAsikki(),soruCek.get(a).getBsikki()+"&"+soruCek.get(a).getCsikki()+"&"+soruCek.get(a).getDsikki()));
                soruDizi.add(sorudizi10);

                a = r.nextInt(soruSayisi);
                ArrayList<sorular> sorudizi11 = new ArrayList<>();
                sorudizi11.add(birSoruOlustur(soruCek.get(a).getSoru(),soruCek.get(a).getAsikki(),soruCek.get(a).getBsikki()+"&"+soruCek.get(a).getCsikki()+"&"+soruCek.get(a).getDsikki()));
                soruDizi.add(sorudizi11);

                a = r.nextInt(soruSayisi);
                ArrayList<sorular> sorudizi12 = new ArrayList<>();
                sorudizi12.add(birSoruOlustur(soruCek.get(a).getSoru(),soruCek.get(a).getAsikki(),soruCek.get(a).getBsikki()+"&"+soruCek.get(a).getCsikki()+"&"+soruCek.get(a).getDsikki()));
                soruDizi.add(sorudizi12);

                a = r.nextInt(soruSayisi);
                ArrayList<sorular> sorudizi13 = new ArrayList<>();
                sorudizi13.add(birSoruOlustur(soruCek.get(a).getSoru(),soruCek.get(a).getAsikki(),soruCek.get(a).getBsikki()+"&"+soruCek.get(a).getCsikki()+"&"+soruCek.get(a).getDsikki()));
                soruDizi.add(sorudizi13);

                a = r.nextInt(soruSayisi);
                ArrayList<sorular> sorudizi14 = new ArrayList<>();
                sorudizi14.add(birSoruOlustur(soruCek.get(a).getSoru(),soruCek.get(a).getAsikki(),soruCek.get(a).getBsikki()+"&"+soruCek.get(a).getCsikki()+"&"+soruCek.get(a).getDsikki()));
                soruDizi.add(sorudizi14);

                a = r.nextInt(soruSayisi);
                ArrayList<sorular> sorudizi15 = new ArrayList<>();
                sorudizi15.add(birSoruOlustur(soruCek.get(a).getSoru(),soruCek.get(a).getAsikki(),soruCek.get(a).getBsikki()+"&"+soruCek.get(a).getCsikki()+"&"+soruCek.get(a).getDsikki()));
                soruDizi.add(sorudizi15);
            }

            @Override
            public void onFailure(Call<List<Model>> call, Throwable t) {

            }
        });






    }

    public sorular birSoruOlustur(String s1,String s2,String s3){

        sorular c1 = new sorular();
        c1.setIcerik(s1);
        c1.setDogrucevap(s2);
        c1.setYanlisCevap(s3);
        return c1;
    }
}
