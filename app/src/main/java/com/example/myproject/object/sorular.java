package com.example.myproject.object;

import java.util.ArrayList;

public class sorular {
    private String icerik, dogrucevap;
    private ArrayList<String> yanlisCevap;

    public String getIcerik() {
        return icerik;
    }

    public void setIcerik(String icerik) {
        this.icerik = icerik;
    }

    public String getDogrucevap() {
        return dogrucevap;
    }

    public void setDogrucevap(String dogrucevap) {
        this.dogrucevap = dogrucevap;
    }

    public ArrayList<String> getYanlisCevap() {
        return yanlisCevap;
    }

    public void setYanlisCevap(ArrayList<String> yanlisCevap) {
        this.yanlisCevap = yanlisCevap;
    }

    public void setYanlisCevap(String yanlisCevaps) {
        String arrA[] = yanlisCevaps.split("&");
        this.yanlisCevap = new ArrayList<>();
        for (String a: arrA){
            yanlisCevap.add(a);
        }
    }
}
