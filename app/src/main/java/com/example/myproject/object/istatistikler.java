package com.example.myproject.object;

import java.util.ArrayList;

public class istatistikler {
    private String sorunumarasi, odulmiktari, zaman;

    public String getSorunumarasi() {
        return sorunumarasi;
    }

    public void setSorunumarasi(String sorunumarasi) {
        this.sorunumarasi = sorunumarasi;
    }

    public String getOdulmiktari() {
        return odulmiktari;
    }

    public void setOdulmiktari(String odulmiktari) {
        this.odulmiktari = odulmiktari;
    }

    public String getZaman() {
        return zaman;
    }

    public void setZaman(String zaman) {
        this.zaman = zaman;
    }
}
