package com.example.myproject;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class istatistik extends AppCompatActivity {

    String url = "http://ebrarguzel.com/milyoner/";
    ListView mlistView;
    private ArrayList<istatistikModel> models;
    private istatistikAdapter adapter;
    ImageView menu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_istatistik);
        mlistView = findViewById(R.id.mListView);
        menu = findViewById(R.id.menu);
        getSoru();
    }
    public void getSoru(){
        Retrofit retrofit = new Retrofit.Builder().baseUrl(url).addConverterFactory(GsonConverterFactory.create()).build();
        myapi api = retrofit.create(myapi.class);
        Call<List<istatistikModel>> call = api.getSorunumarasi();
        call.enqueue(new Callback<List<istatistikModel>>() {
            @Override
            public void onResponse(Call<List<istatistikModel>> call, Response<List<istatistikModel>> response) {
                if(!response.isSuccessful()){

                    return;
                }


                models = new ArrayList<istatistikModel>();
                mlistView = (ListView) findViewById(R.id.mListView);
                models = (ArrayList<istatistikModel>) response.body();
                adapter = new istatistikAdapter(istatistik.this,models);
                mlistView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<istatistikModel>> call, Throwable t) {

            }
        });

    }
}