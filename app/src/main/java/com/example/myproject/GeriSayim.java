package com.example.myproject;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class GeriSayim extends Dialog {
    public GeriSayim(Context context){
        super(context);
        requestWindowFeature(Window.FEATURE_CONTEXT_MENU);
        setContentView(R.layout.activity_geri_sayim);
        Button gerisayimbutton;
        TextView txtbasliyor;
        LottieAnimationView ltview;
        txtbasliyor = findViewById(R.id.txtbasliyor);
        ltview = findViewById(R.id.animation_view);
        /*gerisayimbutton = findViewById(R.id.gerisayimbutton);*/

        txtbasliyor.setVisibility(View.INVISIBLE);
        new CountDownTimer(3000, 1000) {

            public void onTick(long millisUntilFinished) {
                /*gerisayimbutton.setText(""+millisUntilFinished / 1000);*/
            }

            public void onFinish() {
                ltview.setVisibility(View.INVISIBLE);
                txtbasliyor.setVisibility(View.VISIBLE);
            }

        }.start();


    }

}