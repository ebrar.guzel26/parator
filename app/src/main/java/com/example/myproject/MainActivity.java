package com.example.myproject;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;


public class MainActivity extends AppCompatActivity {
    MediaPlayer player;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        MediaPlayer ring= MediaPlayer.create(MainActivity.this,R.raw.ring);
        ring.start();


    }

    public void basla(View view) {
        new GeriSayim(MainActivity.this).show();
        new CountDownTimer(4000, 1000) {
            public void onTick(long millisUntilFinished) {

            }

            public void onFinish() {
                Intent gecisYap = new Intent(MainActivity.this,MainActivity2.class);
                startActivity(gecisYap);

            }

        }.start();

    }


    public void soruYonetim(View view) {

        Intent soruEkle = new Intent(this,Panel.class);
        startActivity(soruEkle);

    }

    @Override
    public void onBackPressed() {


    }


    public void istatistik(View view) {
        Intent istatistik = new Intent(this,istatistik.class);
        startActivity(istatistik);
    }
}