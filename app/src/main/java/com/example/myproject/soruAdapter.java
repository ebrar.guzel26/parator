package com.example.myproject;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;



import java.util.ArrayList;


public class soruAdapter extends ArrayAdapter<Model> {

    private final LayoutInflater inflater;
    private final Context context;
    private ViewHolder holder;
    private final ArrayList<Model> sorularim;

    public soruAdapter(Context context, ArrayList<Model> sorular) {
        super(context,0, sorular);
        this.context = context;
        this.sorularim = sorular;
        inflater = LayoutInflater.from(context);
    }
    @Override
    public int getCount() {
        return sorularim.size();
    }

    @Override
    public Model getItem(int position) {
        return sorularim.get(position);
    }

    @Override
    public long getItemId(int position) {
        return sorularim.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {

            convertView = inflater.inflate(R.layout.list_view_item, null);

            holder = new ViewHolder();
            holder.adaptersoruid = (TextView) convertView.findViewById(R.id.adaptersoruid);
            holder.adaptersoru = (TextView) convertView.findViewById(R.id.adaptersoru);
            holder.adapterasikki = (TextView) convertView.findViewById(R.id.adapterasikki);
            holder.adapterbsikki = (TextView) convertView.findViewById(R.id.adapterbsikki);
            holder.adaptercsikki = (TextView) convertView.findViewById(R.id.adaptercsikki);
            holder.adapterdsikki = (TextView) convertView.findViewById(R.id.adapterdsikki);
            convertView.setTag(holder);

        }
        else{

            holder = (ViewHolder)convertView.getTag();
        }

        Model model = sorularim.get(position);
        if(model != null){

            holder.adaptersoruid.setText("ID: " +String.valueOf(model.getId()));
            holder.adaptersoru.setText("Soru: " +model.getSoru());
            holder.adapterasikki.setText("A) " +model.getAsikki());
            holder.adapterbsikki.setText("B) " +model.getBsikki());
            holder.adaptercsikki.setText("C) " +model.getCsikki());
            holder.adapterdsikki.setText("D) " +model.getDsikki());

        }
        return convertView;
    }

    //View Holder Pattern for better performance
    private static class ViewHolder {
        TextView adaptersoruid;
        TextView adaptersoru;
        TextView adapterasikki;
        TextView adapterbsikki;
        TextView adaptercsikki;
        TextView adapterdsikki;

    }

}
