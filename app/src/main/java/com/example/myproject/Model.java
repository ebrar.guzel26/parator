package com.example.myproject;

public class Model {


    int id;
    String soru,asikki,bsikki,csikki,dsikki;

    public Model(int id, String soru,String asikki,String bsikki,String csikki,String dsikki) {
        this.id = id;
        this.soru = soru;
        this.asikki = asikki;
        this.bsikki = bsikki;
        this.csikki = csikki;
        this.dsikki = dsikki;

    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSoru() {
        return soru;
    }

    public void setSoru(String soru) {
        this.soru = soru;
    }

    public String getAsikki() {
        return asikki;
    }

    public void setAsikki(String asikki) {
        this.asikki = asikki;
    }

    public String getBsikki() {
        return bsikki;
    }

    public void setBsikki(String bsikki) {
        this.bsikki = bsikki;
    }

    public String getCsikki() {
        return csikki;
    }

    public void setCsikki(String csikki) {
        this.csikki = csikki;
    }

    public String getDsikki() {
        return dsikki;
    }

    public void setDsikki(String dsikki) {
        this.dsikki = dsikki;
    }
}
